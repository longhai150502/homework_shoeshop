import React, { Component } from 'react'

export default class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>
            <img style={{ width: "100px" }} src={item.image} alt="" />
          </td>
          <td>{item.name}</td>
          <td>
            <button 
              onClick={() => { this.props.handleDecrement(item) }}
              className='btn-dark m-1'><i class="fa-solid fa-minus"></i></button>
              {item.number}
            <button
              onClick={() => { this.props.handleIncrement(item) }}
              className='btn-dark m-1'><i class="fa-solid fa-plus"></i></button>
          </td>
          <td>${item.price * item.number}</td>
          <td><button onClick={() => { this.props.deleteCartItem(item) }} className='btn btn-danger'>X</button></td>
        </tr>
      );
    })
  }
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Image</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{this.renderCart()}</tbody>
      </table>
    )
  }
}
