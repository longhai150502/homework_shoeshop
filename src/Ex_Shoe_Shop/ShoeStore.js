import React, { Component } from 'react'
import {dataShoe} from './dataShoe'
import Cart from './Cart'
import ProductList from './ProductList'
import DetailShoe from './DetailShoe'

export default class ShoeStore extends Component {
    state = {
        shoeArr: dataShoe,
        detail: [],
        cart: [],
    }
    handleViewDetail = (value) => {
        this.setState({detail: value});
    }
    handleAddToCart = (item) => {
        let cloneCart = [...this.state.cart];
        let index = this.state.cart.findIndex(element => {
            return element.id == item.id;
        })
        if(index == -1) {
            let cartItem = {...item, number:1}
            cloneCart.push(cartItem);
        } else {
            cloneCart[index].number++;
        }
        this.setState({
            cart: cloneCart,
        })
    }
    handleIncrement = (item) =>{
        let cloneCart = [...this.state.cart];
        let index = this.state.cart.findIndex(element => {
            return element.id == item.id
        })
        if(index == -1){
            let cartItem = {...item, number:1}
            cloneCart.push(cartItem);
        }else {
            cloneCart[index].number++;
        }
        this.setState({
            cart: cloneCart,
        })
    }
    handleDecrement = (item) =>{
        let cloneCart = [...this.state.cart];
        let index = this.state.cart.findIndex(element => {
            return element.id == item.id
        })
        if(index == -1){
            let cartItem = {...item, number:1}
            cloneCart.push(cartItem);
        }else if(cloneCart[index].number >= 2) {
            cloneCart[index].number--;
        }
        this.setState({
            cart: cloneCart,
        })
    }
    deleteCartItem = (item) => {
        let cloneCart = [...this.state.cart];
        let index = this.state.cart.findIndex(element => {
            return element.id == item.id
        })
        cloneCart.splice(index,1);
        this.setState({
            cart: cloneCart,
        })
    }
  render() {
    return (
        <div className='d-flex p-5'>
            <div className="nav flex-column nav-pills m-5" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a className="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</a>
                <a className="nav-link" id="v-pills-detail-tab" data-toggle="pill" href="#v-pills-detail" role="tab" aria-controls="v-pills-detail" aria-selected="false">Detail</a>
                <a className="nav-link" id="v-pills-cart-tab" data-toggle="pill" href="#v-pills-cart" role="tab" aria-controls="v-pills-cart" aria-selected="false">Cart</a>
            </div>    
            <div className="tab-content" id="v-pills-tabContent">
                <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <ProductList
                        handleAddToCart = {this.handleAddToCart}
                        handleViewDetail = {this.handleViewDetail}
                        shoeArr = {this.state.shoeArr}>
                    </ProductList>
                </div>
                <div className="tab-pane fade show active" id="v-pills-detail" role="tabpanel" aria-labelledby="v-pills-detail-tab">
                    <DetailShoe detail = {this.state.detail}></DetailShoe>
                </div>
                <div className="tab-pane fade" id="v-pills-cart" role="tabpanel" aria-labelledby="v-pills-cart-tab">
                    <Cart
                        deleteCartItem = {this.deleteCartItem}
                        handleDecrement = {this.handleDecrement}
                        handleIncrement = {this.handleIncrement}
                        cart={this.state.cart} ></Cart>
                </div>
            </div>
        </div>

    )
  }
}
