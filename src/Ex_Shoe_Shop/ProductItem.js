import React, { Component } from 'react'

export default class ProductItem extends Component {
  render() {
    let {image, name, price} = this.props.data
    return (
        <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h5 className='price'>Price: ${price}</h5>
            <button
              onClick={() =>{this.props.handleViewDetail(this.props.data)}}
             className='btn btn-primary mr-5'>Detail</button>
            <button
              onClick={() => {this.props.handleAddToCart(this.props.data)}}
              className='btn btn-dark'>Add to Cart</button>
          </div>
        </div>
      </div>
    )
  }
}
