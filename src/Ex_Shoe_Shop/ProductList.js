import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class ProductList extends Component {
    renderListShoe = () => { 
        return this.props.shoeArr.map((item) => {
            return (
                <ProductItem
                  handleAddToCart = {this.props.handleAddToCart}
                  handleViewDetail = {this.props.handleViewDetail}
                  data = {item} >
                </ProductItem>
            )
        })
     }
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
